### What is this repository for? ###

This repository contains a stripped down version of the Mfgtools2. It is used to perform initial boot and firmware flashing of the BVD3 i.MX7d hardware.

# Demo Package Installation Guide #

### Installation RAM Boot ###
cd $BASE_DIR    
cd mfgtools/    
./local_update.sh    
./mfgtoolcli_linux_start.sh (check for correct jumper settings)    

### Installation Flash Boot ###
cd $BASE_DIR    
cd mfgtools/    
./local_update.sh    
./mfgtoolcli_flash_start.sh (check for corrcet jumper settings)    
