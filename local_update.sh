#!/bin/sh

# This script updates files in this repository from "private_stuff" repository found in ../private_stuff

# Update zImage kernel image
if [ -f ../private-stuff/kernel/install/zImage ]; then
	echo "Updating zImage"
	cp -a ../private-stuff/kernel/install/zImage Profiles/BVD3/OS\ Firmware/files/zImage
fi

# Update u-boot image
if [ -f ../private-stuff/u-boot-imx/u-boot.imx ]; then
	echo "Updating u-boot.imx"
	cp -a ../private-stuff/u-boot-imx/u-boot.imx Profiles/BVD3/OS\ Firmware/files/u-boot.imx
fi

# Update DTB file
if [ -f ../private-stuff/device_tree/bvd3-imx7d.dtb ]; then
	echo "Updating DTB"
	cp -a ../private-stuff/device_tree/bvd3-imx7d.dtb Profiles/BVD3/OS\ Firmware/files/bvd3-imx7d.dtb
fi

# Update Mfgtool initramfs
if [ -f ../private-stuff/initramfs/initramfs-mfgtool.u-boot ]; then
	echo "Updating mfgtools-initramfs"
	cp -a ../private-stuff/initramfs/initramfs-mfgtool.u-boot Profiles/BVD3/OS\ Firmware/files/initramfs-mfgtool.u-boot
fi


# Update Mfgtool initramfs (Flash boot)
if [ -f ../private-stuff/initramfs/rootfs.tar.xz ]; then
	echo "Updating rootfs.tar.xz"
	rm Profiles/BVD3/OS\ Firmware/files/rootfs.tar.xz	
	cp -a ../private-stuff/initramfs/rootfs.tar.xz Profiles/BVD3/OS\ Firmware/files/rootfs.tar.xz
fi

ls -l Profiles/BVD3/OS\ Firmware/files/*

exit 0

